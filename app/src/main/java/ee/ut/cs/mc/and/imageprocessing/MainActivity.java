package ee.ut.cs.mc.and.imageprocessing;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Path;
import android.media.browse.MediaBrowser;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.jabistudio.androidjhlabs.filter.BoxBlurFilter;
import com.jabistudio.androidjhlabs.filter.util.AndroidUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    private static final String HEROKU_URL = "https://cloudimageprocessingapp.herokuapp.com";

   // private static final String HEROKU_URL_LOCAL ="http://10.0.2.2:5000";// "https://cloudimageprocessingapp.herokuapp.com";

    private static final int IMAGE_PICK_CODE = 100;
    Uri imageuri;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }


    private void pickImage(){
        Log.d("__tekrajch","buttonclick");
        Intent intent = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == 100 && (grantResults[0] == PackageManager.PERMISSION_GRANTED)){
            pickImage();
        }else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},100);
            }
        }
    }



    public void localButtonClicked(View v){
        Toast.makeText(this, "Blurring image...", Toast.LENGTH_LONG).show();
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.raw.kutsu_juku);
        new LocalTask().execute(bitmap);
    }
    public void cloudButtonClicked(View v){

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},100);
                return;
            }
        }
        pickImage();

//        Log.d("Image_ha_tekraj",imageuri+"");

        Log.d("__tekrajch","buttonclick");
        //Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.raw.kutsu_juku);

    }



    private void makeHttpRequest(String uri, String content_type){
        final File file = new File(uri);
       // Log.d("__tekrajchh",file+"");
        final String content_types  = content_type;

        final String file_path = file.getAbsolutePath();
      //  Log.d("__tekrajch>>h",file_path+"");
        new  AsyncTask<Void, Void, File>() {
            @Override
            protected File doInBackground(Void... voids) {

                OkHttpClient client = new OkHttpClient();
                Log.d("__tekrajch>>h","working");
                RequestBody file_body = RequestBody.create(MediaType.parse(content_types),file);
                Log.d("__tekrajch>>h","working--"+file_body);
//                RequestBody request_body = new MultipartBody.Builder()
//                        .setType(MultipartBody.FORM)
//                        .addFormDataPart("type",content_types)
//                        .addFormDataPart("file",file_path.substring(file_path.lastIndexOf("/")+1), file_body)
//                        .build();

                Log.d("__tekrajchh>>cont",content_types+"");
                Request request = new Request.Builder()
                        .url("http://172.31.160.197:5000/hello/tekraj")
                        .get()
                        .build();

                try {
                    Response response = client.newCall(request).execute();

                    if(!response.isSuccessful()){
                        throw new IOException("Error : "+response);
                    }
                    Log.d("__tekrajchh","success"+response);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Log.d("__tekrajchh",request+"");
                Call call = client.newCall(request);
                try {
                    Response response = call.execute();
                    Log.d("__tekrajchh","respones");
                    Log.d("__tekrajchh", "doInBackground() called with: " + "params = [" + response.body().string() + "]");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        };




    }

    public String getRealPathFromURI(Uri contentUri) {

        // can post image
        String [] proj={MediaStore.Images.Media.DATA};
        Cursor cursor = getApplicationContext().getContentResolver().query(contentUri,
                proj, // Which columns to return
                null,       // WHERE clause; which rows to return (all rows)
                null,       // WHERE clause selection arguments (none)
                null); // Order-by clause (ascending by name)
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == IMAGE_PICK_CODE){

            Toast.makeText(this, "Sending Image to cloud", Toast.LENGTH_LONG).show();
            imageuri = data.getData();


           // Log.d("__tekrajchhet",path);
            Uri uri = null;
            if (imageuri != null) {
                uri = imageuri;
                String content_type = getContentResolver().getType(imageuri);
                final String path = getRealPathFromURI(imageuri);
                // Log.i("__tekrajchh", "Uri: " + mimeType);


                final File file = new File(path);
                // Log.d("__tekrajchh",file+"");
                final String content_types = content_type;

                final String file_path = file.getAbsolutePath();
                //  Log.d("__tekrajch>>h",file_path+"");


                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        OkHttpClient client =  new OkHttpClient.Builder()
                                .connectTimeout(120, TimeUnit.SECONDS)
                                .writeTimeout(120, TimeUnit.SECONDS)
                                .readTimeout(300, TimeUnit.SECONDS)
                                .build();
                        Log.d("__tekrajch>>h", "working");
                        RequestBody file_body = RequestBody.create(MediaType.parse(content_types), file);
                        Log.d("__tekrajch>>h", "working--" + file_body);
                        RequestBody request_body = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("type",content_types)
                        .addFormDataPart("file",file_path.substring(file_path.lastIndexOf("/")+1), file_body)
                        .build();

                        Log.d("__tekrajchh>>cont", content_types + "");
                        Request request = new Request.Builder()
                                .url(HEROKU_URL+"/simple")
                                .post(request_body)
                                .build();

                        try {
                            Response response = client.newCall(request).execute();

                            if (!response.isSuccessful()) {
                                String er = String.valueOf( new IOException(String.valueOf(response)));
                               Log.d("__tekrajError",er);

                            }else{
                                InputStream inputStream = response.body().byteStream();
                                Bitmap bitmap  = BitmapFactory.decodeStream(inputStream);
//
                                Log.d("__tekrajchh", "success" + bitmap);
                                new CloudTask().execute(bitmap);
                            }



                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                    }
                });

                t.start();

            }
        }
    }

    /** Our abstract AsyncTask for image processing.
     *  It will display the processing result in a UI dialog.
     *  Any implementation of this abstract class must define the
     *  processImage() method, see the @LocalTask example below
     */
    abstract class ImageProcessingAsyncTask extends AsyncTask<Bitmap, Void, Bitmap> {

        abstract Bitmap processImage(Bitmap inputImage);

        protected Bitmap doInBackground(Bitmap... bitmaps) {
            return processImage(bitmaps[0]);
        }

        @Override
        protected void onPostExecute(Bitmap bmp) {
            super.onPostExecute(bmp);
            showImage(bmp);
        }
    }

    /** Send file to Heroku app and display the response image in UI */
    private class CloudTask extends ImageProcessingAsyncTask {
        //TODO
        @Override
        protected Bitmap processImage(Bitmap inputBitmap) {
            Log.d("__tekrajchh<<<<<<<>>>>",inputBitmap+"");
//            Toast.makeText(getApplicationContext(), "Getting converted image from cloud", Toast.LENGTH_LONG).show();
            return inputBitmap;
        }


    }

    /** --- Code below this comment does not need to be changed --- */

    private class LocalTask extends ImageProcessingAsyncTask {

        @Override
        protected Bitmap processImage(Bitmap bmp) {
            int width = bmp.getWidth();
            int height = bmp.getHeight();
            int[] pixels = AndroidUtils.bitmapToIntArray(bmp);

            BoxBlurFilter boxBlurFilter = new BoxBlurFilter();
            boxBlurFilter.setRadius(10);
            boxBlurFilter.setIterations(10);
            int[] result = boxBlurFilter.filter(pixels, width, height);

            return Bitmap.createBitmap(result, width, height, Bitmap.Config.ARGB_8888);
        }
    }


    /** Opens a pop-up dialog displaying the argument Bitmap image */
    public void showImage(Bitmap bmp) {
        Dialog builder = new Dialog(this);
        ImageView imageView = new ImageView(this);

        //Scale down the bitmap to avoid bitmap memory errors, set it to the imageview.
        //This code scales it such that the width is 1280
        int nh = (int) ( bmp.getHeight() * (1280.0 / bmp.getWidth()) );
        Bitmap scaled = Bitmap.createScaledBitmap(bmp, 1280, nh, true);
        imageView.setImageBitmap(scaled);

        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        builder.show();
    }
}