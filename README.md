
# Image Processing Appication

This app lets user select the image and send it to cloud for processing. The processed result will be handeled i.e. shown in the app based on response.

## Getting Started

Clone the app in android studio and run it.

### Prerequisites

All the requirement for developing android application using Java.

 

### Installing

* Android Studio
* Android SDK
* Java (for android)

 
## Snapshots
![](images/a.png)
![](images/b.png)



## Built With

* [Image Processing API](https://bitbucket.org/tekrajchhetri/image-processing-api) - The self written image processing API hosted in heroku cloud.
* [Gradle](https://gradle.org/) - Dependency Management
* [Android Studio](https://developer.android.com/studio) - IDE to develop and test android application

